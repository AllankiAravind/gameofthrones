package framework.gameofthrones.Targaryen.entity;

import javax.persistence.*;

/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
@Entity
@Table(name="testconfiginfo")
public class TestConfigInfoEntity  extends BaseEntity{

	private static final long serialVersionUID = -7935740292813924508L;
	public enum TYPE { PACKAGE, CLASS, GROUP }

    @Column(name = "testconfig_id_fk", nullable = false, length=11, unique=false)
	private Long testconfigidfk;

	@Column(name = "value", nullable = true, length=500)
	private String value;
	
	@Column(name = "type", nullable = true, length=500)
	@Enumerated(EnumType.STRING)
	private TYPE type;
	
	@Column(name = "enabled", nullable = false)
	private Boolean enabled;
	
	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Long getTestconfig() {
		return testconfigidfk;
	}

	public void setTestconfig(Long testconfigidfk) {
		this.testconfigidfk = testconfigidfk;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public TYPE getType() {
		return type;
	}

	public void setType(TYPE type) {
		this.type = type;
	}

	
	
}
