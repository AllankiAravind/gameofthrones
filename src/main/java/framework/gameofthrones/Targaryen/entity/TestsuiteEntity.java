package framework.gameofthrones.Targaryen.entity;

import javax.persistence.*;
import java.util.List;


/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
@Entity
@Table(name="testsuite")
public class TestsuiteEntity extends BaseEntity {

	private static final long serialVersionUID = 1853079958146047839L;
	public enum RENEW_CYCLE { DAY, WEEK, MONTH, YEAR }

    @Column(name = "testsuitename", nullable = false, length=50, unique=true)
    private String testsuitename;
	
	@Column(name = "enabled", nullable = false, length=1)
    private Boolean enabled;
	
	@Column(name = "progress", nullable = false, length=1)
    private Integer progress;
	
	@Column(name = "team", nullable = true, length=100)
    private String team;
	
	
	@Column(name = "core", nullable = true, length=100)
    private String core;
	

	@Column(name = "type", nullable = true, length=100)
    private String type;
	
	@Column(name = "aggregated", nullable = true, length=1)
    private Boolean aggregated;
	
	@Column(name = "aggregatedby", nullable = true)
	@Enumerated(EnumType.STRING)
    private RENEW_CYCLE aggregatedBy;
		

	@OneToMany(targetEntity=TestcaseEntity.class, fetch=FetchType.EAGER)
	@JoinColumn(columnDefinition="testsuite_id_fk", name="testsuite_id_fk", referencedColumnName="id")
	private List<TestcaseEntity> testcases;

	public List<TestcaseEntity> getTestcases() {
		return testcases;
	}

	public String getTestsuitename() {
		return testsuitename;
	}

	public void setTestsuitename(String testsuitename) {
		this.testsuitename = testsuitename;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	public Integer getProgress() {
		if(progress == null){
			return new Integer(0);
		}
		return progress;
	}

	public void setProgress(Integer progress) {
		this.progress = progress;
	}
	
	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}
	
	public String getCore() {
		return core;
	}

	public void setCore(String core) {
		this.core = core;
	}
	
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Boolean getAggregated() {
		if(aggregated == null){
			return Boolean.FALSE;
		}
		return aggregated;
	}

	public void setAggregated(Boolean aggregated) {
		this.aggregated = aggregated;
	}
	
	public RENEW_CYCLE getAggregatedBy() {
		return aggregatedBy;
	}

	public void setAggregatedBy(RENEW_CYCLE aggregatedBy) {
		this.aggregatedBy = aggregatedBy;
	}
	
}
