package framework.gameofthrones.Targaryen.entity;

import javax.persistence.*;
import java.util.Date;

/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
@Entity
@Table(name="testresult")
@SecondaryTable(name="testcase", pkJoinColumns = @PrimaryKeyJoinColumn(name = "id"))
public class TestresultEntity  extends BaseEntity{

	private static final long serialVersionUID = 8825875190264892206L;
	
	@Column(name = "testcase_id_fk", nullable = false, length=20)
	@JoinColumn(table="testcase",nullable=false, referencedColumnName="id", insertable=true, updatable=true)
    private Long testcaseidfk;
	
	@Column(name = "result", nullable = true, length=2)
    private Integer result;
	
	@Column(name = "testrun_id_fk", nullable = false, length=20)
	@JoinColumn(table="testrun",nullable=false, referencedColumnName="id", insertable=true, updatable=true)
    private Long testrunid;
	
	
	@Column(name = "error", nullable=true,length=4000)
    private String error;
	
	@Column(name = "errorMessage", nullable=true, length=200)
	private String errorMessage;
	
	@Column(name = "testparams", nullable = true, length=2000)
	private String testparams;
	
	@Column(table = "testcase")
	private String testcase_id;
	
	@Column(table = "testcase")
	private String description;
	
	@Column(table = "testcase")
	private String groups;

	@Column(name = "updated_by", updatable = true, length = 50)
    protected String updatedBy;
	
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "starttime", nullable = true, insertable = true, updatable = true)
    protected Date starttime;
	
	@Temporal(TemporalType.TIMESTAMP)
    @Column(name = "endtime", nullable = true, insertable = true, updatable = true)
    protected Date endtime;
	
	@Column(name = "comments", nullable=true, length=200)
	private String comments;
	
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}


	public String getTestparams() {
		return testparams;
	}

	public void setTestparams(String testparams) {
		this.testparams = testparams;
	}

	public Long getTestcaseid() {
		return testcaseidfk;
	}

	public void setTestcaseid(Long testcaseid) {
		this.testcaseidfk = testcaseid;
	}

	public Long getTestrunid() {
		return testrunid;
	}

	public void setTestrunid(Long testrunid) {
		this.testrunid = testrunid;
	}

	public Date getStarttime() {
		return starttime;
	}

	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}

	public Date getEndtime() {
		return endtime;
	}

	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}
	
	public String getTestcasedescription() {
		return description;
	}
	
	public String getTestcasemethodname() {
		return testcase_id;
	}
	
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public String getGroups() {
		return groups;
	}
	
}
