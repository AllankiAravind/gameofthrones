package framework.gameofthrones.Targaryen.domain;

import java.util.Date;

/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
public abstract class BaseEntry {

    private static final long serialVersionUID = -4952611614109034209L;

    private Long id;
    private String createdBy;
    private Date createdOn;
    private Date lastModifiedOn;
    protected Long version;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the lastModifiedOn
     */
    public Date getLastModifiedOn() {
        return lastModifiedOn;
    }

    /**
     * @param lastModifiedOn the lastModifiedOn to set
     */
    public void setLastModifiedOn(Date lastModifiedOn) {
        this.lastModifiedOn = lastModifiedOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @param id
     * @param createdOn
     * @param lastModifiedOn
     */
    public BaseEntry(Long id, String createdBy, Date createdOn, Date lastModifiedOn) {
        super();
        this.id = id;
        this.createdBy = createdBy;
        this.createdOn = createdOn;
        this.lastModifiedOn = lastModifiedOn;
    }

    public BaseEntry() {
        super();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("BaseEntry [id=").append(id).append(", createdBy=")
                .append(createdBy).append(", createdOn=").append(createdOn)
                .append(", lastModifiedOn=").append(lastModifiedOn)
                .append(", version=").append(version).append("]");
        return builder.toString();
    }
}