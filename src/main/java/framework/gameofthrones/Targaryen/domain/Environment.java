package framework.gameofthrones.Targaryen.domain;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
@XmlRootElement(name = "environments")
public class Environment  extends BaseEntry{

	private static final long serialVersionUID = -4020546281771953430L;
	
	private String environment;
	private String key;

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
}
