package framework.gameofthrones.Targaryen.domain;

import org.apache.commons.lang3.StringUtils;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;


/**
 * @author abhijit.p
 * @since 12 Feb 2018
 */
@XmlRootElement(name = "user")
public class User  extends BaseEntry{

	private static final long serialVersionUID = -240466578040212747L;
	private String email;
	private List<String> roles;
	private Boolean account_not_locked;
	private Boolean account_not_expired;
	private String username;
	private Boolean enabled;
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public List<String> getRole() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
	
	public Boolean getAccount_not_locked() {
		return account_not_locked;
	}
	
	public void setAccount_not_locked(Boolean account_not_locked) {
		this.account_not_locked = account_not_locked;
	}
	
	public Boolean getAccount_not_expired() {
		return account_not_expired;
	}
	
	public void setAccount_not_expired(Boolean account_not_expired) {
		this.account_not_expired = account_not_expired;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	
	@XmlElement(name="roles")
	public String getRoles() {
		return StringUtils.join(roles, ",");
	}
	
}
