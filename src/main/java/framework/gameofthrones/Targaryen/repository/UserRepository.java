package framework.gameofthrones.Targaryen.repository;

import framework.gameofthrones.Targaryen.entity.UserEntity;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author abhijit.p
 * @since 13 Feb 2018
 */
@CacheConfig(cacheNames="usercache")
public interface UserRepository extends JpaRepository<UserEntity,Long>{
	
	String FIND_BY_EMAIL_QUERY = "Select * from ((Select u.* from users u where u.email = :email) as A left join roles r on A.role_id_fk = r.id)";

	@Override
	UserEntity getOne(Long id);
	
	@Override
	@Cacheable(keyGenerator="customKey")
	List<UserEntity> findAll();
	
	@Cacheable(keyGenerator="customKey")
	@Query(value=FIND_BY_EMAIL_QUERY,nativeQuery = true)
	UserEntity findByEmailIgnoreCase(@Param("email") String email);
	
	@Override
	@CacheEvict(beforeInvocation=true, allEntries=true)
	<S extends UserEntity> S save(S entities);
	
	@Override
	@CacheEvict(beforeInvocation=true, allEntries=true)
	void delete(UserEntity entity);
	
}
