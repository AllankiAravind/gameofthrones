package framework.gameofthrones.Targaryen.repository;


import framework.gameofthrones.Targaryen.entity.TestcaseEntity;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * @author abhijit.p
 * @since 13 Feb 2018
 */
@CacheConfig(cacheNames="testcasecache")
public interface TestcaseRepository extends JpaRepository<TestcaseEntity, Long>{
	

	@Cacheable
	List<TestcaseEntity> findByTestcasenameIgnoreCase(String testcaseid);
	
	@Cacheable
	TestcaseEntity findByTestcasenameAndTestsuiteid(String testcasename, Long testsuiteid);
	
	@Cacheable(key="#p0")
	List<TestcaseEntity> findByTestsuiteid(Long testsuiteid);
	
	@Cacheable
	@Override
	Optional<TestcaseEntity> findById(Long id);
	
	@Override
	@CacheEvict(beforeInvocation=true, allEntries=true)
	<S extends TestcaseEntity> S save(S entities);
	
	@Override
	@CacheEvict(beforeInvocation=true, allEntries=true)
	<S extends TestcaseEntity> S saveAndFlush(S entities);
	
	@Override
	@CacheEvict(beforeInvocation=true, allEntries=true)
	void delete(TestcaseEntity entity);
	
}
