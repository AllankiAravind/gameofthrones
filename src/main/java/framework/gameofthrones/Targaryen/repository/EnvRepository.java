package framework.gameofthrones.Targaryen.repository;


import framework.gameofthrones.Targaryen.entity.EnvEntity;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author abhijit.p
 * @since 13 Feb 2018
 */
@CacheConfig(cacheNames="envcache")
public interface EnvRepository extends JpaRepository<EnvEntity, Long>{

	@Override
	@Cacheable(keyGenerator="customKey")
	List<EnvEntity> findAll();
	
	@Override
	@CacheEvict(beforeInvocation=true, allEntries=true)
	<S extends EnvEntity> S save(S entity);
	
	@Override
	@CacheEvict(beforeInvocation=true, allEntries=true)
	<S extends EnvEntity> S saveAndFlush(S entity);
	
}
