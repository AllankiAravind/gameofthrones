
package framework.gameofthrones.hudor;

import org.codehaus.jackson.annotate.JsonProperty;
import org.neo4j.driver.v1.exceptions.DatabaseException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class HodorPage {

    @JsonProperty("testrun_id")
    public String testrunId;
    @JsonProperty("pod_name")
    public String podName;
    @JsonProperty("executed_by")
    public String executedBy;
    @JsonProperty("executed_at")
    public String executedAt;
    @JsonProperty("tc_ids")
    public List<TcId> tcIds = null;

    @JsonProperty("testrun_id")
    public String getTestrunId() {
        return testrunId;
    }

    @JsonProperty("testrun_id")
    public void setTestrunId(String testrunId) {
        this.testrunId = testrunId;
    }


    public HodorPage withTestrunId(String testrunId) {
        this.testrunId = testrunId;
        return this;
    }

    @JsonProperty("pod_name")
    public String getPodName() {
        return podName;
    }

    @JsonProperty("pod_name")
    public void setPodName(String podName) {
        this.podName = podName;
    }

    public HodorPage withPodName(String podName) {
        this.podName = podName;
        return this;
    }

    @JsonProperty("executed_by")
    public String getExecutedBy() {
        return executedBy;
    }

    @JsonProperty("executed_by")
    public void setExecutedBy(String executedBy) {
        this.executedBy = executedBy;
    }

    public HodorPage withExecutedBy(String executedBy) {
        this.executedBy = executedBy;
        return this;
    }

    @JsonProperty("executed_at")
    public String getExecutedAt() {
        return executedAt;
    }

    @JsonProperty("executed_at")
    public void setExecutedAt(String executedAt) {
        this.executedAt = executedAt;
    }

    public HodorPage withExecutedAt(String executedAt) {
        this.executedAt = executedAt;
        return this;
    }

    @JsonProperty("tc_ids")
    public List<TcId> getTcIds() {
        return tcIds;
    }

    @JsonProperty("tc_ids")
    public void setTcIds(List<TcId> tcIds) {
        this.tcIds = tcIds;
    }

    public HodorPage withTcIds(List<TcId> tcIds) {
        this.tcIds = tcIds;
        return this;
    }
    public HodorPage setDefaultData(String tcid,String testrunId,String status,String pod,String serviceName,String groupName,String automationTime)
    {
        TcId tcId = new TcId();
        tcId.withId(tcid);
        tcId.withStatus(status);
        tcId.withGroup(groupName);
        tcId.withService(serviceName);
        tcId.withManHours(automationTime);
        List<TcId> data= new ArrayList<TcId>();
        data.add(tcId);
        if(testrunId==null)
            return this.withPodName(pod).withTcIds(data).withExecutedAt(getDateInReadableFormatWithSecond());

        else return this.withPodName(pod).withTestrunId(testrunId).withTcIds(data).withExecutedAt(getDateInReadableFormatWithSecond());
    }
    public HodorPage setDefaultDPData(String tcid,String dpid,String testrunId,String status,String pod,String automationTime)
    {
        TcId tcId = new TcId();
        tcId.withId(tcid);
        tcId.withStatus(status);
        tcId.withManHours(automationTime);
        Value value=new Value();
        value.withDpId(dpid).withStatus(status);
        List<Value> valData= new ArrayList<Value>();
        valData.add(value);
        tcId.withValue(valData);
        List<TcId> data= new ArrayList<TcId>();
        data.add(tcId);

        return this.withPodName(pod).withTestrunId(testrunId).withTcIds(data).withExecutedAt(getDateInReadableFormatWithSecond());
    }

    public static String getDateInReadableFormatWithSecond()
    {
        SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        return sdf.format(c.getTime());
    }

}
