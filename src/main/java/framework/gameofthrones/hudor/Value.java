
package framework.gameofthrones.hudor;

import org.codehaus.jackson.annotate.JsonProperty;

public class Value {

    @JsonProperty("dp_id")
    private String dpId;
    @JsonProperty("status")
    private String status;
    @JsonProperty("payload")
    private String payload;
    @JsonProperty("message")
    private String message;

    @JsonProperty("dp_id")
    public String getDpId() {
        return dpId;
    }

    @JsonProperty("dp_id")
    public void setDpId(String dpId) {
        this.dpId = dpId;
    }

    public Value withDpId(String dpId) {
        this.dpId = dpId;
        return this;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    public Value withStatus(String status) {
        this.status = status;
        return this;
    }

    @JsonProperty("payload")
    public String getPayload() {
        return payload;
    }

    @JsonProperty("payload")
    public void setPayload(String payload) {
        this.payload = payload;
    }

    public Value withPayload(String payload) {
        this.payload = payload;
        return this;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    public Value withMessage(String message) {
        this.message = message;
        return this;
    }
}
