
package framework.gameofthrones.hudor;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;


public class TcId {

    @JsonProperty("id")
    private String id;
    @JsonProperty("status")
    private String status;
    @JsonProperty("payload")
    private String payload;
    @JsonProperty("message")
    private String message;
    @JsonProperty("value")
    private List<Value> value = null;


    public String getMan_hours() {
        return automation_name;
    }

    public void setMan_hours(String man_hours) {
        this.automation_name = man_hours;
    }

    @JsonProperty("automation_time")
    private String automation_name;

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public TcId withManHours(String automationName) {
        this.automation_name = automationName;
        return this;
    }
    public TcId withService(String service) {
        this.service = service;
        return this;
    }

    public TcId withGroup(String group) {
        this.group = group;
        return this;
    }


    @JsonProperty("service")
    private String service;

    @JsonProperty("group")
    private String group;

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public TcId withId(String id) {
        this.id = id;
        return this;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    public TcId withStatus(String status) {
        this.status = status;
        return this;
    }

    @JsonProperty("payload")
    public String getPayload() {
        return payload;
    }

    @JsonProperty("payload")
    public void setPayload(String payload) {
        this.payload = payload;
    }

    public TcId withPayload(String payload) {
        this.payload = payload;
        return this;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    public TcId withMessage(String message) {
        this.message = message;
        return this;
    }

    @JsonProperty("value")
    public List<Value> getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(List<Value> value) {
        this.value = value;
    }

    public TcId withValue(List<Value> value) {
        this.value = value;
        return this;
    }

}
