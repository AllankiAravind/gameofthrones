package framework.gameofthrones.Tyrion;

public class CreateDynamoDBConfigBuilder {
    public String partitionKey;
    public String sortKey;
    public String tableName;
    public int readCapacity;
    public int writeCapacity;

    public CreateDynamoDBConfigBuilder() {
    }

    public CreateDynamoDBConfigBuilder setPartitionKey(String partitionKey) {
        this.partitionKey = partitionKey;
        return this;
    }

    public CreateDynamoDBConfigBuilder setsSortKey(String sortKey) {
        this.sortKey = sortKey;
        return this;
    }

    public CreateDynamoDBConfigBuilder setTableName(String tableName) {
        this.tableName = tableName;
        return this;
    }

    public CreateDynamoDBConfigBuilder setReadCapacity(int readCapacity) {
        this.readCapacity = readCapacity;
        return this;
    }

    public CreateDynamoDBConfigBuilder setWriteCapacity(int writeCapacity) {
        this.writeCapacity = writeCapacity;
        return this;
    }

    public CreateDynamoDBConfig build() {
        return new CreateDynamoDBConfig(this.tableName, this.partitionKey, this.sortKey, this.readCapacity, this.writeCapacity);
    }

    public class CreateDynamoDBConfig {
        public String partitionKey;
        public String sortKey;
        public String tableName;
        public int readCapacity;
        public int writeCapacity;

        private CreateDynamoDBConfig(String tableName, String partitionKey, String sortKey, int readCapacity, int writeCapacity) {
            this.partitionKey = partitionKey;
            this.sortKey = sortKey;
            this.tableName = tableName;
            this.readCapacity = readCapacity;
            this.writeCapacity = writeCapacity;
        }
    }
}

