package framework.gameofthrones.Tyrion;

public interface AWSInfraClient {
    enum ConnType {
        SHUTTLE,
        UAT
    }

    /**
     * Connection type can be Shuttle/UAT based on which account the infra component resides
     */
    void initializeClient(ConnType connType);
}
