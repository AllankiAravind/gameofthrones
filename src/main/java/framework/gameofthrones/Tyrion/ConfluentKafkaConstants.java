package framework.gameofthrones.Tyrion;

import java.util.HashMap;
import java.util.Map;

public class ConfluentKafkaConstants {

    public enum ClusterType{
        KAFKA_TXN_PRIMARY,
        KAFKA_BATCH_PRIMARY,
        KAFKA_TXN_HA_PRIMARY,
        KAFKA_TXN_HA_SECONDARY
    }

    public static Map<ClusterType, String> ConfluentServerEndpoints = new HashMap<ClusterType, String>() {{
        put(ClusterType.KAFKA_TXN_PRIMARY, "pkc-l9wvm.ap-southeast-1.aws.confluent.cloud:9092");
        put(ClusterType.KAFKA_BATCH_PRIMARY, "pkc-l9wvm.ap-southeast-1.aws.confluent.cloud:9092");
        put(ClusterType.KAFKA_TXN_HA_PRIMARY, "pkc-l9wvm.ap-southeast-1.aws.confluent.cloud:9092");
        put(ClusterType.KAFKA_TXN_HA_SECONDARY, "pkc-l7pr2.ap-south-1.aws.confluent.cloud:9092");
    }};
}
