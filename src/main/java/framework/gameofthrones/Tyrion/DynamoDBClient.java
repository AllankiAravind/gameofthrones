package framework.gameofthrones.Tyrion;


import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.*;

import java.util.*;
import java.util.logging.Logger;

/**
 * @author abhijit.p
 * @since May 2020
 */
public class DynamoDBClient implements AWSInfraClient {

    private static final Logger LOG =  Logger.getLogger(DynamoDBClient.class.getSimpleName());

    private DynamoDB dynamoDB;

    @Override
    public void initializeClient(ConnType connType) {
        this.dynamoDB = AWSHelper.getDynamoClient(connType);
    }

    public void createDynamoTable(CreateDynamoDBConfigBuilder.CreateDynamoDBConfig config) {

        //Create Table
        LOG.info("Performing table create for " + config.tableName + ", wait...");

        List<AttributeDefinition> attributeDefinitions = new ArrayList<>();
        List<KeySchemaElement> elements = new ArrayList<>();

        //Primary Partition Key
        attributeDefinitions.add(new AttributeDefinition()
                .withAttributeName(config.partitionKey)
                .withAttributeType(ScalarAttributeType.S));
        elements.add(new KeySchemaElement()
                .withKeyType(KeyType.HASH)
                .withAttributeName(config.partitionKey));

        //Sort Key
        if (config.sortKey != null) {
            attributeDefinitions.add(new AttributeDefinition()
                    .withAttributeName(config.sortKey)
                    .withAttributeType(ScalarAttributeType.S));
            elements.add(new KeySchemaElement()
                    .withKeyType(KeyType.RANGE)
                    .withAttributeName(config.sortKey));
        }

        CreateTableRequest createTableRequest = new CreateTableRequest();
        createTableRequest.setAttributeDefinitions(attributeDefinitions);
        createTableRequest.setKeySchema(elements);
        createTableRequest.setTableName(config.tableName);

        if (config.readCapacity == 0) {
            createTableRequest.setBillingMode("PAY_PER_REQUEST");
        } else {
            createTableRequest.setProvisionedThroughput(new ProvisionedThroughput()
                    .withReadCapacityUnits((long) config.readCapacity)
                    .withWriteCapacityUnits((long) config.writeCapacity));
        }

        this.dynamoDB.createTable(createTableRequest);
        LOG.info("Table created  " + config.tableName);
    }

    /**
     * Delete entry from Dynamo using partitionKey
     */
    public void deleteEntry(String tableName, String partitionKey, String partitionKeyValue) throws Exception {
        Table table = this.dynamoDB.getTable(tableName);
        DeleteItemSpec deleteItemSpec = new DeleteItemSpec().withPrimaryKey(
                new PrimaryKey(partitionKey, partitionKeyValue));
        table.deleteItem(deleteItemSpec);
        LOG.info(String.format("Successfully deleted entry with key=%s on table=%s", partitionKeyValue, tableName));

    }

    /**
     * Delete entry from Dynamo using partitionKey & sortKey
     */
    public void deleteEntry(String tableName, String partitionKey, String partitionKeyValue, String sortKey,
                            String sortKeyValue) throws Exception {
        Table table = this.dynamoDB.getTable(tableName);
        DeleteItemSpec deleteItemSpec = new DeleteItemSpec().withPrimaryKey(
                new PrimaryKey(partitionKey, partitionKeyValue, sortKey, sortKeyValue));
        table.deleteItem(deleteItemSpec);
        LOG.info(String.format("Successfully deleted entry with key=%s and range=%s on table=%s", partitionKeyValue,
                sortKeyValue, tableName));

    }

    /**
     * Returns a dynamo item as a MAP using only primary key
     *
     * @param tableName
     * @param partitionKey
     * @param value
     * @return
     */
    public Map<String, Object> getItemFromDB(String tableName, String partitionKey, Object value) {
        Table table = this.dynamoDB.getTable(tableName);
        Item item = table.getItem(partitionKey, value);
        return item.asMap();
    }

    /**
     * Returns a dynamo item as a MAP using primary+range key
     *
     * @param tableName
     * @param partitionKey
     * @param value
     * @param rangeKey
     * @param rangeValue
     * @return
     */
    public Map<String, Object> getItemFromDB(String tableName, String partitionKey, Object value, String rangeKey, Object rangeValue) {
        Table table = this.dynamoDB.getTable(tableName);
        Item item = table.getItem(partitionKey, value, rangeKey, rangeValue);
        return item.asMap();
    }

    /**
     * Query data(expression) from DynamoDB table using partitionKey with it's value
     */
    public String getValuefromDynamoDB(String tableName, Object partitionKeyValue, String partitionKey, String expression, int maxResult) throws Exception {
        Table table = this.dynamoDB.getTable(tableName);
        QuerySpec spec = new QuerySpec();
        spec.withProjectionExpression(expression)
                .withKeyConditionExpression(partitionKey + " = :v_id")
                .withValueMap(new ValueMap().with(":v_id", partitionKeyValue))
                .withMaxResultSize(maxResult)
                .withScanIndexForward(false);
        return fetchItem(table, expression, spec);
    }

    /**
     * Query data(expression) from DynamoDB table using partitionKey with it's value
     * Use this method if expression name is state in DynamoDb table
     * State is a keyword in QuerySpec
     */
    public String getStateValuefromDynamoDB(String tableName, Object partitionKeyValue, String partitionKey, String expression, int maxResult) throws Exception {
        Table table = this.dynamoDB.getTable(tableName);
        Map<String, String> expressionAttributeNames = new HashMap<String, String>();
        expressionAttributeNames.put("#recover", expression);
        String projectionExpression = "#recover";
        QuerySpec spec = new QuerySpec();
        spec.withProjectionExpression(projectionExpression)
                .withKeyConditionExpression(partitionKey + " = :v_id")
                .withValueMap(new ValueMap().with(":v_id", partitionKeyValue))
                .withNameMap(expressionAttributeNames)
                .withMaxResultSize(maxResult)
                .withScanIndexForward(false);
        return fetchItem(table, expression, spec);
    }

    private String fetchItem(Table table, String expression, QuerySpec spec) {
        ItemCollection<QueryOutcome> items = table.query(spec);
        Iterator<Item> iterator = items.iterator();
        while (iterator.hasNext()) {
            Item item = iterator.next();
            if (item != null && item.get(expression) != null)
                return item.get(expression).toString();
        }
        return null;
    }

}

