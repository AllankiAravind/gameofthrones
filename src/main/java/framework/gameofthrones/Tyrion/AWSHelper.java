package framework.gameofthrones.Tyrion;

import com.amazonaws.auth.*;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.securitytoken.AWSSecurityTokenService;
import com.amazonaws.services.securitytoken.AWSSecurityTokenServiceClientBuilder;
import com.amazonaws.services.securitytoken.model.AssumeRoleRequest;
import com.amazonaws.services.securitytoken.model.AssumeRoleResult;
import com.amazonaws.services.securitytoken.model.Credentials;

import java.io.File;

public class AWSHelper {

    AmazonS3 s3client;
    public String bucketName;
    private final static String roleArn = "arn:aws:iam::213343865855:role/shuttle-it-user";
    private final static String defaultAWSRegion = "ap-southeast-1";
    private final static String defaultAWSDynamoEndpoint = "dynamodb.ap-southeast-1.amazonaws.com";

    public void createConn() {
        AWSCredentials credentials = new BasicAWSCredentials(
                System.getenv("AWS_ACCESS_KEY_ID"),
                System.getenv("AWS_SECRET_ACCESS_KEY")
        );
        s3client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.AP_SOUTHEAST_1)
                .build();
    }

    public void listObjects() {
        ObjectListing objectListing = s3client.listObjects(bucketName);
        for (S3ObjectSummary os : objectListing.getObjectSummaries()) {
            System.out.println(os.getKey());
        }
    }

    public void uploadObject(String objectKey, String fileName) {
        s3client.putObject(
                bucketName,
                objectKey,
                new File(fileName)
        );
    }

    public void deleteObject(String objectKey) {
        System.out.println("Deleting object : " + objectKey);
        s3client.deleteObject(bucketName, objectKey);
    }

    /**
     * Returns the AWSCredentialsProvider based on connType (Shuttle/UAT)
     *
     * @param connType
     * @return
     */
    public static AWSCredentialsProvider getCredentialsProvider(AWSInfraClient.ConnType connType) {

        AWSStaticCredentialsProvider credentialsProvider = null;

        if (connType == AWSInfraClient.ConnType.SHUTTLE)
            credentialsProvider = new AWSStaticCredentialsProvider(getAWSCreds());

        if (connType == AWSInfraClient.ConnType.UAT)
            credentialsProvider = getRoleBasedCredentials(getAWSCreds(), roleArn);

        return credentialsProvider;

    }

    /**
     * Fetches role based aws credentials (Used currently for the UAT account)
     *
     * @param credentials
     * @param roleArn
     * @return
     */
    private static AWSStaticCredentialsProvider getRoleBasedCredentials(AWSCredentials credentials, String roleArn) {

        //Get role based creds
        AWSSecurityTokenService stsClient = AWSSecurityTokenServiceClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.AP_SOUTHEAST_1)
                .build();

        AssumeRoleResult roleResponse = stsClient.assumeRole(new AssumeRoleRequest()
                .withRoleArn(roleArn)
                .withRoleSessionName("Sample"));
        Credentials sessionCredentials = roleResponse.getCredentials();

        return new AWSStaticCredentialsProvider(new BasicSessionCredentials(
                sessionCredentials.getAccessKeyId(),
                sessionCredentials.getSecretAccessKey(),
                sessionCredentials.getSessionToken()
        ));
    }

    /**
     * s
     * Fetches aws keys from env variables
     */
    public static AWSCredentials getAWSCreds() {
        return new BasicAWSCredentials(
                System.getenv("AWS_ACCESS_KEY_ID"),
                System.getenv("AWS_SECRET_ACCESS_KEY")
        );
    }

    /**
     * Fetches aws regionn from env variables
     */
    public static String getAWSRegion() {
        String region = System.getenv("AWS_REGION");
        return region == null ? defaultAWSRegion : region;
    }

    /**
     * Fetches aws service endpoint from env variables
     */
    public static String getServiceEndpoint() {
        String endpoint = System.getenv("AWS_SERVICE_ENDPOINT");
        return endpoint == null ? defaultAWSDynamoEndpoint : endpoint;
    }

    /**
     * Creates Dynamo DB client
     *
     * @param connType
     * @return
     */
    public static DynamoDB getDynamoClient(AWSInfraClient.ConnType connType) {
        AmazonDynamoDB amazonDynamoDB = AmazonDynamoDBClientBuilder.standard()
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(
                        AWSHelper.getServiceEndpoint(),
                        AWSHelper.getAWSRegion()))
                .withCredentials(getCredentialsProvider(connType))
                .build();
        return new DynamoDB(amazonDynamoDB);
    }

    /**
     * Creates an Amazonn s3 client
     *
     * @param connType
     * @return
     */
    public static AmazonS3 getS3Client(AWSInfraClient.ConnType connType) {
        return AmazonS3ClientBuilder
                .standard()
                .withCredentials(getCredentialsProvider(connType))
                .withRegion(AWSHelper.getAWSRegion())
                .build();
    }
}

