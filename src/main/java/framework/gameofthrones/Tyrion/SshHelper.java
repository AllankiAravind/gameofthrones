package framework.gameofthrones.Tyrion;

import com.jcraft.jsch.*;
import org.apache.log4j.Logger;

public class SshHelper {
    private static final String STRICT_HOST_KEY_CHECKING_KEY = "StrictHostKeyChecking";
    private static final String STRICT_HOST_KEY_CHECKING_VALUE = "no";
    private static final String CHANNEL_TYPE = "shell";

    String sshHost1 = "13.229.147.208";
    String sshUser1 = "ubuntu";

    String sshKeyFile = "/Users/mukesh.kabra/Downloads/perf.pem";

    final Logger logger = Logger.getLogger(SshHelper.class);



    public Session createSession() throws Exception{
        JSch jsch = new JSch();
        jsch.addIdentity(sshKeyFile);
        jsch.setConfig(STRICT_HOST_KEY_CHECKING_KEY, STRICT_HOST_KEY_CHECKING_VALUE);

        logger.info("Attempting connection to " + sshUser1 + "@" + sshHost1);
        Session session = jsch.getSession(sshUser1, sshHost1, 22);
        return session;


    }

    public void connectSession(Session session) throws Exception{
        session.connect();
        int lport = 9092;
        String rhost = "u1-tripmanager-db.u1.swiggy.in";
        int rport = 9092;
        int assinged_port= session.setPortForwardingL(lport, rhost, rport);
       System.out.println("localhost:" + assinged_port + " -> " + rhost + ":" + rport);

        logger.info("Connected to " + sshUser1 + "@" + sshHost1);

    }

    public void disconnectSession(Session session){
        session.disconnect();
        logger.info("DisConnected to " + sshUser1 + "@" + sshHost1);

    }





}
