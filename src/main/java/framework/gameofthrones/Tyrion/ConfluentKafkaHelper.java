package framework.gameofthrones.Tyrion;

import com.swiggy.kafka.clients.configs.AuthMechanism;
import com.swiggy.kafka.clients.configs.CommonConfig;
import com.swiggy.kafka.clients.configs.ProducerConfig;
import com.swiggy.kafka.clients.configs.enums.ProducerAcks;
import com.swiggy.kafka.clients.producer.Producer;

/**
 * Created by prateek.kalita
 */
public class ConfluentKafkaHelper {

    public Producer producer;

    // private constructor restricted to this class itself
    public void startProducer(String bootStrapServer)
    {
        CommonConfig.Cluster primary = CommonConfig.Cluster.builder()
                .bootstrapServers(bootStrapServer)
                .username(System.getenv("CONFLUENT_USERNAME"))
                .password(System.getenv("CONFLUENT_PASSWORD"))
                .authMechanism(AuthMechanism.SASL_PLAIN)
                .build();

        ProducerConfig producerConfig = ProducerConfig.builder()
                .acks(ProducerAcks.ALL)
                .retries(1)
                .primary(primary)
                .clientId("fxm_test")
//				.extraConfigs(extraConfigsProducer)
                .build();

        producer = new Producer(producerConfig);
        //producer.init();
        producer.start();
    }

    // private constructor restricted to this class itself
    public void startProducer(String bootStrapServer,String access, String secret, String clientId)
    {
        CommonConfig.Cluster primary = CommonConfig.Cluster.builder()
                .bootstrapServers(bootStrapServer)
                .username(access)
                .password(secret)
                .authMechanism(AuthMechanism.SASL_PLAIN)
                .build();

        ProducerConfig producerConfig = ProducerConfig.builder()
                .acks(ProducerAcks.ALL)
                .retries(1)
                .primary(primary)
                .clientId(clientId)
//				.extraConfigs(extraConfigsProducer)
                .build();

        producer = new Producer(producerConfig);
        //producer.init();
        producer.start();
    }

    public void sendMessage(String topicName, String message){
        producer.send(topicName, message);
        System.out.println("Produced Msg to topic : " + topicName + " : \n" + message);
    }

    public void closeProducer(){
        producer.stop();
    }
}
