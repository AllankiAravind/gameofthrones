package framework.gameofthrones.Tyrion;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.secretsmanager.AWSSecretsManager;
import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder;
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest;
import com.amazonaws.services.secretsmanager.model.GetSecretValueResult;
import com.swiggy.kafka.clients.configs.AuthMechanism;
import com.swiggy.kafka.clients.configs.CommonConfig;
import com.swiggy.kafka.clients.configs.ProducerConfig;
import com.swiggy.kafka.clients.configs.enums.ProducerAcks;
import com.swiggy.kafka.clients.producer.Producer;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Base64;
import java.util.concurrent.TimeUnit;


/**
 * Created by prateek.kalita
 */
public class ConfluentHelper {

    private final String serviceName = "swiggy-test-framwork";

    public class ConnectionDetails {
        private String API_KEY;
        private String API_SECRET;

        public ConnectionDetails(String API_KEY, String API_SECRET) {
            this.API_KEY = API_KEY;
            this.API_SECRET = API_SECRET;
        }

        public String getAPI_KEY() {
            return API_KEY;
        }

        public String getAPI_SECRET() {
            return API_SECRET;
        }
    }

    private Producer producer;
    private String topicName;

    /**
     * Starts a producer on confluent kafka
     * The kafka name as in the config xml needs to be passed
     * Please NOTE : Please create separate objects of this class for different topics
     */
    public void startProducer(String env, ConfluentKafkaConstants.ClusterType clusterType, String topicName, String clientId) throws Exception {

        this.topicName = topicName;

        //Fetch Keys from Secret Manager
        ConnectionDetails connectionDetails = fetchConnectionDetailsFromSecretManager(env, serviceName, clusterType);

        CommonConfig.Cluster config = CommonConfig.Cluster.builder()
                .bootstrapServers(ConfluentKafkaConstants.ConfluentServerEndpoints.get(clusterType))
                .username(connectionDetails.getAPI_KEY())
                .password(connectionDetails.getAPI_SECRET())
                .authMechanism(AuthMechanism.SASL_PLAIN)
                .build();

        producer = new Producer(ProducerConfig.builder()
                .acks(ProducerAcks.ALL)
                .retries(3)
                .primary(config)
                .clientId(clientId)
                .build());

        producer.start();
        Thread.sleep(6000);
    }

    /**
     * This method can be invoked after startProducer() has been called
     * This method will produce the message to the topic specified
     */
    public void sendMessage(String message) {
        try {
            producer.send(topicName, message).get(10, TimeUnit.SECONDS);
        } catch (Exception e) {
            System.out.println("Exception in sending event : \n" + e);
        }
        System.out.println("Produced Msg to topic : " + topicName + " : \n" + message);
    }

    public void sendMessage(byte[] message) {
        try {
            producer.send(topicName, message).get(10, TimeUnit.SECONDS);
        } catch (Exception e) {
            System.out.println("Exception in sending event : \n" + e);
        }
        System.out.println("Produced Msg to topic : " + topicName + " : \n" + message);
    }

    /**
     * This method can be invoked in the AfterTest to close the kafka producer
     */
    public void closeProducer() {
        producer.stop();
    }

    private ConnectionDetails fetchConnectionDetailsFromSecretManager(String env, String serviceName, ConfluentKafkaConstants.ClusterType clusterType) throws
            IOException {
        String secretName = "/swiggy.cloud/" + env + "/singapore/" + serviceName + "/" + clusterType;
        String region = "ap-southeast-1";
        AWSSecretsManager client = AWSSecretsManagerClientBuilder.standard()
                .withRegion(region)
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(
                        System.getenv("AWS_ACCESS_KEY_ID"),
                        System.getenv("AWS_SECRET_ACCESS_KEY")
                )))
                .build();

        String secret = "";
        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest()
                .withSecretId(secretName);
        GetSecretValueResult getSecretValueResult = null;
        try {
            getSecretValueResult = client.getSecretValue(getSecretValueRequest);
        } catch (Exception e) {
            throw e;
        }
        secret = (getSecretValueResult.getSecretString() != null) ? getSecretValueResult.getSecretString() : new String(Base64.getDecoder().decode(getSecretValueResult.getSecretBinary()).array());
        JSONObject json = new JSONObject(secret);
        System.out.println("Confluent Key : "+json.getString("API_KEY"));
        System.out.println("Confluent Secret : "+json.getString("API_SECRET"));
        return new ConnectionDetails(json.getString("API_KEY"), json.getString("API_SECRET"));
    }


    /**
     * @param env
     * @param serviceName
     * @param clusterType
     * @param connType    use connection Type as "AWSInfraClient.ConnType.UAT"
     * @return
     */
    private ConnectionDetails fetchConnectionDetailsFromSecretManagerForUAT(String env, String serviceName, ConfluentKafkaConstants.ClusterType clusterType, AWSInfraClient.ConnType connType) {
        String secretName = "/swiggy.cloud/" + env + "/singapore/" + serviceName + "/" + clusterType;
        String region = "ap-southeast-1";

        AWSSecretsManager client = AWSSecretsManagerClientBuilder.standard()
                .withRegion(region)
                .withCredentials(AWSHelper.getCredentialsProvider(connType))
                .build();

        String secret;
        GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest()
                .withSecretId(secretName);
        GetSecretValueResult getSecretValueResult;
        try {
            getSecretValueResult = client.getSecretValue(getSecretValueRequest);
        } catch (Exception e) {
            throw e;
        }
        secret = (getSecretValueResult.getSecretString() != null) ? getSecretValueResult.getSecretString() : new String(Base64.getDecoder().decode(getSecretValueResult.getSecretBinary()).array());
        JSONObject json = new JSONObject(secret);
        return new ConfluentHelper.ConnectionDetails(json.getString("API_KEY"), json.getString("API_SECRET"));
    }


    /**
     * @param env
     * @param clusterType
     * @param topicName
     * @param clientId
     * @param connType    use connection Type as AWSInfraClient.ConnType.UAT
     * @throws Exception
     */
    public void startProducer(String env, ConfluentKafkaConstants.ClusterType clusterType, String topicName, String clientId, AWSInfraClient.ConnType connType) throws Exception {

        this.topicName = topicName;

        //Fetch Keys from Secret Manager
        ConnectionDetails connectionDetails = fetchConnectionDetailsFromSecretManagerForUAT(env, serviceName, clusterType, connType);

        CommonConfig.Cluster config = CommonConfig.Cluster.builder()
                .bootstrapServers(ConfluentKafkaConstants.ConfluentServerEndpoints.get(clusterType))
                .username(connectionDetails.getAPI_KEY())
                .password(connectionDetails.getAPI_SECRET())
                .authMechanism(AuthMechanism.SASL_PLAIN)
                .build();

        producer = new Producer(ProducerConfig.builder()
                .acks(ProducerAcks.ALL)
                .retries(3)
                .primary(config)
                .clientId(clientId)
                .build());

        producer.start();
        Thread.sleep(6000);

    }
}


