package framework.gameofthrones.JonSnow;

public enum AcceptType {
    XML,
    JSON,
    FORMDATA,
    URLENCODED,
    TEXT_PLAIN
}