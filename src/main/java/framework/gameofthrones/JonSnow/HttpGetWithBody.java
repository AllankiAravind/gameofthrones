package framework.gameofthrones.JonSnow;


import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;

public class HttpGetWithBody extends HttpEntityEnclosingRequestBase {
    public static final String METHOD_NAME = "GET";
    public static final String APPLICATION_XML = "application/xml";
    public static final String APPLICATION_JSON = "application/json";
    public static final String TEXT_PLAIN = "text/plain";
    public static final String TEXT_XML = "text/xml";
    public static final String TEXT_HTML = "text/html";
    public static final String APPLICATION_X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";

    public String getMethod() {
        return METHOD_NAME;
    }

    public HttpGetWithBody(final String uri) {
        super();
        setURI(URI.create(uri));
    }

    public HttpGetWithBody(final URI uri) {
        super();
        setURI(uri);
    }

    public HttpGetWithBody() {
        super();
    }



    public CloseableHttpResponse sendGet(String URL, String PARAMS, HashMap<String, String> header) throws IOException
    {
        CloseableHttpClient httpclient = HttpClients.createDefault();

        HttpGetWithBody httpGet = new HttpGetWithBody(URL);
        for (String key : header.keySet())
        {

            httpGet.addHeader(key,header.get(key));

        }
        switch(header.get("Content-Type")){
        case APPLICATION_XML:
            StringEntity input = new StringEntity(PARAMS, ContentType.APPLICATION_XML);
            httpGet.setEntity(input);
            break;
        case APPLICATION_JSON:
            input = new StringEntity(PARAMS, ContentType.APPLICATION_JSON);
            httpGet.setEntity(input);
            break;
        case TEXT_PLAIN:
            input = new StringEntity(PARAMS, ContentType.TEXT_PLAIN);
            httpGet.setEntity(input);
            break;
        case TEXT_XML:
            input = new StringEntity(PARAMS, ContentType.TEXT_XML);
            httpGet.setEntity(input);
            break;
        case TEXT_HTML:
            input = new StringEntity(PARAMS, ContentType.TEXT_HTML);
            httpGet.setEntity(input);
            break;
        case APPLICATION_X_WWW_FORM_URLENCODED:
            input = new StringEntity(PARAMS, ContentType.APPLICATION_FORM_URLENCODED);
            httpGet.setEntity(input);
            break;
        }

        Header requestHeaders[] = httpGet.getAllHeaders();
        System.out.println(requestHeaders.length);
        CloseableHttpResponse response = httpclient.execute(httpGet);
        return response;

    }
}