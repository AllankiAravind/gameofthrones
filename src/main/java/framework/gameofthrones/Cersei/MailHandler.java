package framework.gameofthrones.Cersei;

/**
 * Created by ashish.bajpai on 29/10/17.
 */


import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.StaticData;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

public class MailHandler
{
    private static HtmlEmail mail;
    private StaticData staticdata = new StaticData();
    static Logger log = Logger.getLogger(MailHandler.class);
    Initialize myconfig = null;
    private static EmailAttachment attachment = new EmailAttachment();
    private String emaillist = "";


    public MailHandler(Initialize config, String finalreport){
        myconfig = config;
        staticdata = new StaticData(myconfig.Properties);
        mail = PrepareRawMailer("ashish.bajpai@swiggy.in","","[Test Execution Report] - ERP - OMS - Sanity - Failed", MailTemplate.TESTRUNREPORT, finalreport);
    }


    public MailHandler(Initialize config){
        myconfig = config;
        staticdata = new StaticData(myconfig.Properties);
        mail = PrepareRawMailer("ashish.bajpai@swiggy.in","","[Test Execution Report] - ERP - OMS - Sanity - Failed", MailTemplate.TESTRUNREPORT, "this is test report");
    }


    public MailHandler(String finalreport, String EmailList){
        //myconfig = config;
        emaillist = EmailList;
        staticdata = new StaticData();
        mail = PrepareRawMailer(emaillist,"","[Test Execution Report] - ERP - OMS - Sanity - Failed", MailTemplate.TESTRUNREPORT, finalreport);
    }

    public MailHandler(MailData data, String finalreport) {
        emaillist = data.MailMeta.ToEmails;
        staticdata = new StaticData();
        mail = PrepareRawMailer(data.MailMeta.ToEmails,"",data.MailMeta.SubjectLine, MailTemplate.TESTRUNREPORT, finalreport);
    }

    public MailHandler( String receipients,String finalreport, String fileName, List<String> attachmentFiles) {
//        mail = PrepareRawMailer_search("vijay.s@swiggy.in,nevia.naveli@swiggy.in,siva.subramanian@swiggy.in","","Production Utility Results For search", MailTemplate.TESTRUNREPORT, finalreport,fileName,attachmentFiles);
        mail = PrepareRawMailer_search(receipients,"","[Production] Crawler Utility Results For Search & Suggest", MailTemplate.TESTRUNREPORT, finalreport,fileName,attachmentFiles);
    }

    private HtmlEmail PrepareRawMailer_search(String reciepients, String bccreciepients, String Subject, MailTemplate Mailtype, String htmlmailbody, String fileName, List<String> attachmentFiles){
        try{
            HtmlEmail mail = MailerConfig();
            System.out.println("reciepients = " + reciepients);
            mail.setFrom("swiggytest@swiggy.in", "Swiggy Test Engineering");
            String[] recipients = reciepients.split(",");
            for (int i = 0; i < recipients.length; i++)
            {
                mail.addTo(recipients[i]);
            }

            System.out.println("fileName = " + fileName);
            File file = new File(fileName);

            mail.setHtmlMsg(htmlmailbody);
            if(file.exists())
            {
                mail.attach(file);
            }

            if(attachmentFiles.size()>0)
            {
                for(int i=0 ; i < attachmentFiles.size(); i++)
                {
                    File file1 = new File(attachmentFiles.get(i));
                    mail.attach(file1);
                }

            }
            mail.setSubject(Subject);
            mail.send();
            System.out.println("Mail successfully send");
        }
     catch (EmailException e) {
         e.printStackTrace();
     }
    return mail;
    }

    private HtmlEmail PrepareRawMailer(String reciepients, String bccreciepients, String Subject, MailTemplate Mailtype, String htmlmailbody){
        try{
            HtmlEmail mail = MailerConfig();
            System.out.println("reciepients = " + reciepients);
            mail.setFrom("swiggytest@swiggy.in", "Swiggy Test Engineering");
            String[] recipients = reciepients.split(",");
            for (int i = 0; i < recipients.length; i++)
            {
                mail.addTo(recipients[i]);
            }
            //mail.addTo(reciepients);
            URL url = new URL("https://www.google.com/a/swiggy.in/images/logo.gif?alpha=1&service=google_default");
            try {
                String cid = mail.embed(url, "Swiggy logo");
            }
            catch(Exception e){
                System.out.println("Not able to fetch logo for mail");
            }

            mail.setHtmlMsg(htmlmailbody);
            mail.setSubject(Subject);
//            mail.send();
            System.out.println("Mail successfully send");
        }
        catch (EmailException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return mail;
    }


    private HtmlEmail MailerConfig(){
        HtmlEmail email = new HtmlEmail();
        email.setHostName("smtp.gmail.com");
        email.setSmtpPort(587);
        email.setAuthenticator(new DefaultAuthenticator("swiggytest@swiggy.in", "swiggytest@123"));
        email.setTLS(true);
        return email;
    }

    /**
     * Generic Mail Handler
     * @param subject
     * @param mailContent
     * @param attachments
     * @param recipientList
     * @param sentFrom - comma separated 2 values - email , name
     */
    public MailHandler( String recipientList,String subject, String mailContent, List<String> attachments,String sentFrom) {
        mail =  prepareRawMailer(recipientList,subject,mailContent,attachments,sentFrom);
    }

    /***
     * getting mail object
     * @param subject
     * @param mailContent
     * @param attachments
     * @param recipientList
     * @param sentFrom - comma separated 2 values - email , name
     * @return
     */
    private HtmlEmail prepareRawMailer(String recipientList,String subject, String mailContent, List<String> attachments,String sentFrom ) {
        try {
            HtmlEmail mail = MailerConfig();
            String[] from = sentFrom.split(",");
            mail.setFrom(from[0],from[1]);
            for (String recipient : Arrays.asList(recipientList.split(",")))
                mail.addTo(recipient);
            mail.setSubject(subject);
            mail.setHtmlMsg(mailContent);
            for (String file : attachments)
                mail.attach(new File(file));
            mail.send();
            log.info("Email Sent to : "+ recipientList);
        }catch (Exception ex) {
            log.error("Error sending mail to :"+recipientList);
        }
        return mail;
    }

}
