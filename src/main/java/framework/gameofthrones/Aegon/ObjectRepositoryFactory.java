package framework.gameofthrones.Aegon;

/**
 * Created by ashish.bajpai on 27/04/17.
 */
public interface ObjectRepositoryFactory {
    Element GetElement(String elementname);
    Element[] GetFullRepository();
}
