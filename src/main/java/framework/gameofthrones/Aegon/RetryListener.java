package framework.gameofthrones.Aegon;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.*;
import org.testng.annotations.ITestAnnotation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.*;

/**
 * @author abhijit.p
 * @since 23 Jan 2018
 */
public class RetryListener extends TestListenerAdapter implements IAnnotationTransformer {
    private static Logger log = LoggerFactory.getLogger(RetryListener.class);
    TestUtil testUtil=new TestUtil();
    @Override
    public void transform(ITestAnnotation testAnnotation, Class testClass,
                          Constructor testConstructor, Method testMethod)	{
        IRetryAnalyzer retry = testAnnotation.getRetryAnalyzer();

        if (retry == null)	{
            testAnnotation.setRetryAnalyzer(RetryAnalyzer.class);
        }

    }

    @Override
    public void onFinish(ITestContext testContext) {
        super.onFinish(testContext);

        // List of test results which we will delete later
        List<ITestResult> testsToBeRemoved = new ArrayList<>();

        // collect all id's from passed test
        Set<Integer> passedTestIds = new HashSet<>();
        for (ITestResult passedTest : testContext.getPassedTests().getAllResults()) {
            passedTestIds.add(TestUtil.getId(passedTest));
        }

        Set<Integer> failedTestIds = new HashSet<>();
        for (ITestResult failedTest : testContext.getFailedTests().getAllResults()) {

            // id = class + method + dataprovider
            int failedTestId = TestUtil.getId(failedTest);

            // if we saw this test as a failed test before we mark as to be deleted
            // or delete this failed test if there is at least one passed version
            if (failedTestIds.contains(failedTestId) || passedTestIds.contains(failedTestId)) {
                testsToBeRemoved.add(failedTest);
            } else {
                failedTestIds.add(failedTestId);
            }
        }

        // finally delete all tests that are marked
        for (Iterator<ITestResult> iterator = testContext.getFailedTests().getAllResults().iterator(); iterator.hasNext(); ) {
            ITestResult testResult = iterator.next();
            if (testsToBeRemoved.contains(testResult)) {
                iterator.remove();
            }
        }
    }
}


//    @Override
//    public void onFinish(ITestContext context) {
//        Iterator<ITestResult> failedTestCases =context.getFailedTests().getAllResults().iterator();
//        while (failedTestCases.hasNext()) {
//            System.out.println("failedTestCases");
//            ITestResult failedTestCase = failedTestCases.next();
//            ITestNGMethod method = failedTestCase.getMethod();
//            if (context.getFailedTests().getResults(method).size() > 1) {
//                System.out.println("failed test case remove as dup:" + failedTestCase.getTestClass().toString());
//                failedTestCases.remove();
//            } else {
//
//                if (context.getPassedTests().getResults(method).size() > 0) {
//                    System.out.println("failed test case remove as pass retry:" + failedTestCase.getTestClass().toString());
//                    failedTestCases.remove();
//                }
//            }
//        }
//    }