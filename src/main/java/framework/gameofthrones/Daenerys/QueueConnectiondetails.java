package framework.gameofthrones.Daenerys;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

public class QueueConnectiondetails {


    private String hostname;
    private String hosturl;
    private String username;
    private String password;
    private int port;
    private List<Queue> queues;


    @XmlAttribute(name = "hosturl")
    public void setHosturl(String hosturl) {
        this.hosturl = hosturl;
    }

    public String getHosturl() {
        return hosturl;
    }

    @XmlAttribute(name = "hostname")
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getHostname() {
        return hostname;
    }

    @XmlAttribute(name = "port", required=false)
    public void setPort(int port) {
        this.port = port;
    }

    @XmlAttribute(name = "username", required=false)
    public void setUsername(String username) {
        this.username = username;
    }

    @XmlAttribute(name = "password", required=false)
    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }



    public int getPort(){
        if(this.port == 0){
            return 5672;
        }
        return port;
    }

    public List<Queue> getQueues() {
        return queues;
    }

    @XmlElement(name="queue")
    public void setQueues(List<Queue> queues) {
        this.queues = queues;
    }



    public Queue GetQueueDetails(String queueName){
        Queue queue = new Queue();
        for (Queue q: queues) {
            if (q.getName().trim().toLowerCase().equals(queueName)){
                queue = q;
            }
        }
        return queue;
    }
}
