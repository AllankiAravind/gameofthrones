package framework.gameofthrones.Daenerys;

import framework.gameofthrones.Aegon.PropertiesHandler;

import java.io.IOException;

/**
 * Created by ashish.bajpai on 08/09/15.
 */
public class InitializeEnvironmentObject {
    public Setup setup;

    public InitializeEnvironmentObject() throws IOException
    {
        setup = new Setup();
    }

    public InitializeEnvironmentObject(PropertiesHandler properties) throws IOException
    {
        setup = new Setup(properties);
    }

    public InitializeEnvironmentObject(String environmentname) throws IOException
    {
        setup = new Setup(environmentname);
    }



}
