package framework.gameofthrones.Daenerys;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by ashish.bajpai on 04/02/15.
 */

@XmlRootElement(name = "Environment")
public class environment
{
    private Framework framework;
    private String name;
    private int urls;
    private services services;
    private Database database;
    private RedisConnection redisConnections;
    private KafkaConnection kafkaConnections;
    private Queues queues;
    private WebSocket webSocket;
    private Excel excel;
    private stafdata stafdata;
    
    
    
    
    public stafdata getStafdata() {
		return stafdata;
	}

    @XmlElement(name="stafdata")
	public void setStafdata(stafdata stafdata) {
		this.stafdata = stafdata;
	}

//	public Excel getExcel() {
//		return excel;
//	}
//
//    @XmlElement(name="excel")
//	public void setExcel(Excel excel) {
//		this.excel = excel;
//	}

	@XmlElement(name="database")
    public void setDatabase(Database database) {
        this.database = database;
    }

    public Database getDatabase() {
        return database;
    }

    @XmlElement(name="redis")
    public void setRedisConnection(RedisConnection redisConnections) {
        this.redisConnections = redisConnections;
    }

    @XmlElement(name="kafka")
    public void setKafkaConnection(KafkaConnection kafkaConnections) {
        this.kafkaConnections = kafkaConnections;
    }

    public KafkaConnection getKafkaConnection() {
        return kafkaConnections;
    }

    public RedisConnection getRedisConnection() {
        return redisConnections;
    }

    @XmlElement
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @XmlElement
    public void setUrls(int urls) {
        this.urls = urls;
    }

    public int getUrls() {
        return urls;
    }

    @XmlElement
    public void setFramework(Framework framework) {
        this.framework = framework;
    }

    public Framework getFramework() {
        return framework;
    }

    @XmlElement()
    public void setServices(services services) {
        this.services = services;
    }

    public services getServices() {
        return services;
    }
    
    @XmlElement(name="Queues")
    public void setQueues(Queues queues) {
        this.queues = queues;
    }
    
    public Queues getQueues() {
        return queues;
    }

    public WebSocket getWebSocket() {
        return webSocket;
    }

    @XmlElement(name="websocket")
    public void setWebSocket(WebSocket webSocket) {
        this.webSocket = webSocket;
    }
}
