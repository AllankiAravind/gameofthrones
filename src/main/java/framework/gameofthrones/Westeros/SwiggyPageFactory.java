package framework.gameofthrones.Westeros;

import io.appium.java_client.AppiumDriver;

import java.lang.reflect.*;
import java.util.List;

public class SwiggyPageFactory {

    public enum Direction {
        LEFT,
        RIGHT,
        UP,
        DOWN
    }


    public SwiggyPageFactory() {
    }


    public static void initElements(AppiumDriver driverRef, Object page) {
        final Class<?> proxyIn = page.getClass();
        Field[] fields = proxyIn.getDeclaredFields();
        for (Field field : fields) {
            if (field.getType().toString().contains("interface")) {
                Object value = assignFields(driverRef, page, field);
                field.setAccessible(true);
                if (value != null)
                    try {
                        field.set(page, value);
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
            }

        }
    }


    private static Object assignFields(final AppiumDriver driver, Object page, final Field field) {
        final ClassLoader loader = page.getClass().getClassLoader();
        if (!IElement.class.isAssignableFrom(field.getType()) && !listDecorator(field)) {
            return null;
        } else {
            if (IElement.class.isAssignableFrom(field.getType())) {
                return proxyForLocator(driver, field, loader);
            } else {
                return List.class.isAssignableFrom(field.getType()) ? proxyForListLocator(driver, field, loader) : null;
            }
        }
    }

    private static boolean listDecorator(Field field) {
        if (!List.class.isAssignableFrom(field.getType())) {
            return false;
        } else {
            Type genericType = field.getGenericType();
            if (!(genericType instanceof ParameterizedType)) {
                return false;
            } else {
                Type listType = ((ParameterizedType) genericType).getActualTypeArguments()[0];
                return IElement.class.equals(listType);
            }
        }
    }

    private static IElement proxyForLocator(AppiumDriver driver, Field field, ClassLoader loader) {
        InvocationHandler handler = new LocatingElementHandler(new ElementLocator(driver, field));
        IElement iElement = (IElement) Proxy.newProxyInstance(loader, new Class[]{IElement.class}, handler);
        return iElement;
    }

    private static List<IElement> proxyForListLocator(AppiumDriver driver, Field field, ClassLoader loader) {
        InvocationHandler handler = new LocatingElementListHandler(new ElementLocator(driver, field));
        List<IElement> iElements = (List) Proxy.newProxyInstance( loader, new Class[]{List.class}, handler);
        return iElements;
    }












}