package framework.gameofthrones.Westeros;

public interface RobustTestHelper {

    void uploadApp(String path);

    String getDownloadPath();
}
