package framework.gameofthrones.Westeros;

public class AutomationLocator implements Locator{

    private String type;
    private String id;

    AutomationLocator(String type, String id) {
        this.type = type;
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }
}