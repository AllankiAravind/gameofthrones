package framework.gameofthrones.Westeros;

import io.appium.java_client.AppiumDriver;

public class LaunchManager {

    private static ThreadLocal<AppiumDriver> appiumDriver = new ThreadLocal<>();

    public static AppiumDriver getUiLauncher() {
        return appiumDriver.get();
    }

    public static void setUiLauncher(AppiumDriver driver) {
        appiumDriver.set( driver );
    }
}