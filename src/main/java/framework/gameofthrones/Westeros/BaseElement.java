package framework.gameofthrones.Westeros;

import io.appium.java_client.MobileElement;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class BaseElement< T extends WebDriver, R extends WebElement >  implements IElement
{
	private R webElement;
	private T webDriver;
	private List< R > webElementList;
	final long TIME_OUT = 10;

	BaseElement(T webDriver, R webElement)
	{
		this.webDriver = webDriver;
		this.webElement = webElement;
	}

	public BaseElement( T webDriver, R webElement, List< R > webElementList)
	{
		this.webDriver = webDriver;
		this.webElement = webElement;
		this.webElementList = webElementList;
	}

	@Override
	public void click() {
		webElement.click();
	}

	public void submit()
	{
		webElement.submit();
	}

	public void clear(int... timeouts)
	{
		webElement.clear();
	}

	public String getTagName(int... timeouts)
	{

		return webElement.getTagName();

	}

	public String getAttribute(String name, int... timeouts)
	{
		return webElement.getAttribute(name);

	}

	public boolean isSelected(int... timeouts)
	{

		return webElement.isSelected();
	}

	@Override
	public void sendKeys(String value) {
		    MobileElement element = ( MobileElement ) webElement;
		    System.out.println(  " SETTING VALUE  " + value );
			element.setValue( value );

	}


	public boolean isEnabled(int... timeouts)
	{

		return webElement.isEnabled();
	}

	public String getText(int... timeouts)
	{

		return webElement.getText();
	}

	@Override
	public boolean isDisplayed( ) {
		WebDriverWait webDriverWait = new WebDriverWait( webDriver, this.TIME_OUT );
		return  webDriverWait.until(ExpectedConditions.visibilityOf( webElement )).isDisplayed();
	}

	public R findElement(By by)
	{
		return ( R ) webDriver.findElement(by);
	}


	public Point getLocation(int... timeouts)
	{

		return webElement.getLocation();
	}

	public Dimension getSize(int... timeouts)
	{

		return  webElement.getSize();
	}

	@Override
	public String getCssValue(String propertyName, int... timeouts)
	{

		return webElement.getCssValue(propertyName);
	}

	public void hover(int... timeouts)
	{

	}

	@Override
	public void hoverAndClick(IElement element, int... timeouts) {

	}


	public Select getSelect(int... timeouts)
	{
		return new Select( this.webElement );
	}

	@Override
	public By getByLocator() {
		return null;
	}

	@Override
	public ArrayList<String> getAllText() {

		final ArrayList<String> list = new ArrayList<>();
		Function< WebElement, String> stringFunction = WebElement::getText;
		list.addAll( Optional.ofNullable(webElementList).orElseGet( Collections::emptyList ).stream().map( webElement -> stringFunction.apply( webElement ) ).collect(Collectors.toList()));
		return list;
	}



	@Override
	public boolean isDisplayed(long timeOut) {

		WebDriverWait webDriverWait = new WebDriverWait( webDriver, this.TIME_OUT );
		return  webDriverWait.until(ExpectedConditions.visibilityOf( webElement )).isDisplayed();
	}

	@Override
	public void setValue(CharSequence sequence) {

		webElement.sendKeys( sequence );
	}

	@Override
	public R getWebElement() {
		return this.webElement;
	}


	@Override
	public String[] getSimilarTagsContentList() {
		// TODO Auto-generated method stub
		return null;
	}


}
