package framework.gameofthrones.Westeros;

import com.google.common.base.Function;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.FluentWait;

import java.time.Duration;
import java.util.List;


class FindElementFactory {

    /**
     *
     * @param driver  Webdriver object
     * @param timeout  search timeout for driver to find element.
     * @param pollingDuration Poll for element on device.
     * @param locator common search context interface
     * @param <D> ? extends webdriver
     * @return Webelement
     */

    < D extends WebDriver> WebElement findElement(D driver, long timeout,
                                                              long pollingDuration,
                                                              Locator locator) {
        return new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(timeout))
                .pollingEvery(Duration.ofMillis(pollingDuration))
                .ignoring(NoSuchElementException.class)
                .ignoring(ElementNotVisibleException.class)
                .until((Function<D,WebElement>) localDriver -> {
                    if (locator instanceof ByLocator) {
                        return  driver.findElement(((ByLocator) locator).getBy());
                    } else if (locator instanceof AutomationLocator) {
                        return ((AppiumDriver ) driver).findElement(((AutomationLocator) locator).getId(), ((AutomationLocator) locator).getType());
                    } else if (locator instanceof AccessibilityLocator) {
                        return ((AppiumDriver)driver).findElementByAccessibilityId(((AccessibilityLocator) locator).getId());
                    }
                    return null;
                });
    }

    /**
     *
     * @param driver  Webdriver object
     * @param timeout  search timeout for driver to find element.
     * @param pollingDuration Poll for element on device.
     * @param locator common search context interface
     * @param <D> ? extends webdriver
     * @return List of Webelement
     */

    public <D extends WebDriver> List<WebElement> findElements(D driver, long timeout,
                                                      long pollingDuration,
                                                      Locator locator) {
        return  new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(timeout))
                .pollingEvery(Duration.ofMillis(pollingDuration))
                .ignoring(NoSuchElementException.class)
                .ignoring(ElementNotVisibleException.class)
                .until((Function<D, List<WebElement>>) localDriver -> {
                    if (locator instanceof ByLocator) {
                        return  driver.findElements(((ByLocator) locator).getBy());
                    } else if (locator instanceof AutomationLocator) {
                        return  ((AppiumDriver) driver).findElements(((AutomationLocator) locator).getId(), ((AutomationLocator) locator).getType());
                    } else if (locator instanceof AccessibilityLocator) {
                        return  ((AppiumDriver) driver).findElementsByAccessibilityId(((AccessibilityLocator) locator).getId());
                    }
                    return null;
                });
    }
}
