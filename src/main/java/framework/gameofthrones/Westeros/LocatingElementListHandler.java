package framework.gameofthrones.Westeros;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;


public class LocatingElementListHandler implements InvocationHandler
{
	private final ElementLocator elementLocator;
	private final int DEFAULT_FIND_TIMEOUT = 15;

	public LocatingElementListHandler(ElementLocator elementLocator)
	{
		this.elementLocator = elementLocator;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] objects)
			throws Throwable
	{
		List<IElement> element = this.elementLocator.findElements(DEFAULT_FIND_TIMEOUT );

		if ("getWrappedElement".equals(method.getName()))
			return element;

		try {
			return method.invoke(element, objects);
		} catch (InvocationTargetException e) {
			throw e.getCause();
		}
	}


}
