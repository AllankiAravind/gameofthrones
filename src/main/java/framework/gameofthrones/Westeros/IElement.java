package framework.gameofthrones.Westeros;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;

/**
 * Created by kiran.j on 12/5/17.
 */
public interface IElement< T extends WebElement > {

    void clear(int... timeouts);
    void click();
    T findElement(By by);
    String getAttribute(String name, int... timeouts);
    String getCssValue(String propertyName, int... timeouts);
    Point getLocation(int... timeouts);
    Dimension getSize(int... timeouts);
    String getTagName(int... timeouts);
    String getText(int... timeouts);
    boolean isDisplayed();
    boolean isEnabled(int... timeouts);
    boolean isSelected(int... timeouts);
    void sendKeys(String value);
    void submit();
    String[] getSimilarTagsContentList();
    void hover(int... timeouts);
    void hoverAndClick(IElement element, int... timeouts);
    Select getSelect(int... timeouts);
    By getByLocator();
    ArrayList<String> getAllText();
    boolean isDisplayed(long timeOut);
    void setValue(CharSequence sequence);
    T getWebElement();

}
