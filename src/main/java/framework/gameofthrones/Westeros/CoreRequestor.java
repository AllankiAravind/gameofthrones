package framework.gameofthrones.Westeros;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class CoreRequestor implements RobustTestHelper {

    private final String ROBUS_TEST_URL = "http://devicelab.bundl.in:8085";
    private final String PROJECT_ID = "5d022b953d25030592a3bdf2";
    private final String PROJECT_PATH = "/v3/project/" + PROJECT_ID + "/build" ;
    final String ACCESS_KEY = "BHCTajO17_FeqD49fTRUQIWND4";




    @Override
    public void uploadApp(String path) {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        URI uri = null;
        try {
            uri = new URIBuilder()
                    .setScheme("http")
                    .setHost("10.250.82.26")
                    .setPath(PROJECT_PATH)
                    .setParameter("accesskey", ACCESS_KEY)
                    .build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        File file = new File( path );
        HttpPut httpPut = new HttpPut(uri);
        httpPut.setHeader("Content-type", "multipart/form-data");
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addBinaryBody("build", file, ContentType.create("multipart/form-data"  ), path );
        HttpEntity entity = builder.build();
        httpPut.setEntity( entity );
        try {
            httpclient.execute( httpPut );
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    @Override
    public String getDownloadPath() {
        return null;
    }




    public String read(String[] command) {

        for (String x : command) {
            System.out.println( x );
        }

        String s = null;
        StringBuilder sb = new StringBuilder();
        try {

            Process p = Runtime.getRuntime().exec( command  );
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(
                    p.getErrorStream()));
            while ((s = stdInput.readLine()) != null) {
                System.out.println( s );
                sb.append(s);
            }

            while ((s = stdError.readLine()) != null) {
                System.out.println(s);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return sb.toString();
    }



}
