package framework.gameofthrones.Westeros;

import org.openqa.selenium.By;

public class ByLocator implements Locator {

    private By by;

    ByLocator(By by){
        this.by = by;
    }

    public By getBy() {
        return by;
    }
}
