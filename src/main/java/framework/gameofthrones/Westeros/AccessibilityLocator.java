package framework.gameofthrones.Westeros;

public class AccessibilityLocator implements Locator{

    private String id;

    AccessibilityLocator(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
