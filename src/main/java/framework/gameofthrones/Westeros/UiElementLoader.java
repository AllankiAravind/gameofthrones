package framework.gameofthrones.Westeros;

import com.google.inject.Singleton;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Singleton
/*
 * Class for reading central Element locator for android,IOS device .
 * @Param xml file
 * @return stores the element locator with its value in HashMap.
 *
 * */
public class UiElementLoader {
    private static UiElementLoader instance;
    private String resourceName;
    private XMLConfiguration xmlConfig;
    private HashMap<String, String> uiElements = new HashMap<>();


    static UiElementLoader getInstance(String resourceName) {
        instance = instance != null ? instance : new UiElementLoader(resourceName);
        return instance;
    }

    UiElementLoader(String resourceName) {

        this(new XMLConfiguration(), resourceName);
    }

    UiElementLoader(XMLConfiguration xmlConfig, String resourceName) {
        this.resourceName = resourceName;
        this.xmlConfig = xmlConfig;
        this.load();
        this.build(this.readXML());
    }

    private void load() {
        final String fileName = resourcePath(this.resourceName);
        try {
            this.xmlConfig.load(fileName);
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }


    private void build(List<HierarchicalConfiguration> configurationList) {
        configurationList.forEach(config -> uiElements.put(getAttributeValue(config, "name"), getAttributeValue(config, "value")));
    }

    private String getAttributeValue(HierarchicalConfiguration configuration,
                                     String Attributename) {
        return configuration.getString("[@" + Attributename + "]");
    }

    String getUIElementDetails(String id) {
        return uiElements.entrySet().stream().filter(item -> item.getKey().equals(id)).map(Map.Entry::getValue).findFirst().get();
    }


    private String resourcePath(String resourceName) {

       return System.getProperty("user.dir")+ File.separator + StringConstants.RESOURCE_PATH + StringConstants.UI_ELEMENT_RESOURCE_FOLDER + File.separator+ resourceName;
    }

    List<HierarchicalConfiguration> readXML() {
        return xmlConfig.configurationsAt(StringConstants.XML_KEY);
    }

    /*
     * @param takes tag as Pageclass value. returns only elements associated with it.
     * */

    protected List<HierarchicalConfiguration> readXML(String attribute) {

        return xmlConfig.configurationsAt(attribute);
    }


}


