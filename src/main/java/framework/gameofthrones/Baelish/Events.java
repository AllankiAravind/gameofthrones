package framework.gameofthrones.Baelish;

public class Events {

    private String e;
    private String sn;
    private String on;
    private String op;
    private String ui;
    private String us;
    private String ud;
    private String p;
    private String cx;
    private String av;
    private String sqn;
    private String sc;
    private String rf;
    private String lt;
    private String lg;
    private String ts;
    private String itd;
    private String dml;
    private String dmf;
    private String drd;
    private String vos;
    private String nwt;
    private String nwc;

    public String getGtmcb() {
        return gtmcb;
    }

    public void setGtmcb(String gtmcb) {
        this.gtmcb = gtmcb;
    }

    private String gtmcb;

    public boolean isPresent() {
        return isPresent;
    }

    public void setPresent(boolean present) {
        isPresent = present;
    }

    private boolean isPresent;

    public String getIte() {
        return ite;
    }

    public void setIte(String ite) {
        this.ite = ite;
    }

    private String ite;

    public String getSavc() {
        return savc;
    }

    public void setSavc(String savc) {
        this.savc = savc;
    }

    private String savc;

    public String getNwt() {
        return nwt;
    }

    public void setNwt(String nwt) {
        this.nwt = nwt;
    }

    public String getNwc() {
        return nwc;
    }

    public void setNwc(String nwc) {
        this.nwc = nwc;
    }

    public String getIce() {
        return ice;
    }

    public void setIce(String ice) {
        this.ice = ice;
    }

    public String getPsv() {
        return psv;
    }

    public void setPsv(String psv) {
        this.psv = psv;
    }

    private String ice;
    private String psv;

    public String getVos() {
        return vos;
    }

    public void setVos(String vos) {
        this.vos = vos;
    }




    public String getDrd() {
        return drd;
    }

    public void setDrd(String drd) {
        this.drd = drd;
    }


    public String getDml() {
        return dml;
    }

    public void setDml(String dml) {
        this.dml = dml;
    }


    public String getDmf() {
        return dmf;
    }

    public void setDmf(String dmf) {
        this.dmf = dmf;
    }


    public String getOv() {
        return ov;
    }

    public void setOv(String ov) {
        this.ov = ov;
    }

    private String ov;

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getOn() {
        return on;
    }

    public void setOn(String on) {
        this.on = on;
    }

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public String getUi() {
        return ui;
    }

    public void setUi(String ui) {
        this.ui = ui;
    }

    public String getUs() {
        return us;
    }

    public void setUs(String us) {
        this.us = us;
    }

    public String getUd() {
        return ud;
    }

    public void setUd(String ud) {
        this.ud = ud;
    }

    public String getP() {
        return p;
    }

    public void setP(String p) {
        this.p = p;
    }

    public String getCx() {
        return cx;
    }

    public void setCx(String cx) {
        this.cx = cx;
    }

    public String getAv() {
        return av;
    }

    public void setAv(String av) {
        this.av = av;
    }

    public String getSqn() {
        return sqn;
    }

    public void setSqn(String sqn) {
        this.sqn = sqn;
    }

    public String getSc() {
        return sc;
    }

    public void setSc(String sc) {
        this.sc = sc;
    }

    public String getRf() {
        return rf;
    }

    public void setRf(String rf) {
        this.rf = rf;
    }

    public String getLt() {
        return lt;
    }

    public void setLt(String lt) {
        this.lt = lt;
    }

    public String getLg() {
        return lg;
    }

    public void setLg(String lg) {
        this.lg = lg;
    }

    public String getTs() {
        return ts;
    }

    public void setTs(String ts) {
        this.ts = ts;
    }

    public String getItd() {
        return itd;
    }

    public void setItd(String itd) {
        this.itd = itd;
    }




}
