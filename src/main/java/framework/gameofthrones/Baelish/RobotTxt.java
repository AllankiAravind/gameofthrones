package framework.gameofthrones.Baelish;

import org.jeasy.rules.annotation.Action;
import org.jeasy.rules.annotation.Condition;
import org.jeasy.rules.annotation.Fact;
import org.jeasy.rules.annotation.Rule;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

@Rule(name = "Robot.txt check", description = "Robot.txt is present" )
public class RobotTxt {
	private String targeturl;
    private SEOData seodata;
    public static final String CLASS_NAME="Robot.txt";
    SEOResult sr=new SEOResult();
    
    public RobotTxt(SEOData data){
    	seodata=data;
    	targeturl=seodata.URL;
    }
    
    @Condition
    public boolean isRobotPresent(@Fact("robottxt") boolean robottxt){
        return robottxt;
    }
    
    @Action
    public void checkRobotTxt(){
    	sr.getURL(targeturl);
    	Document doc=null;
    	String robotfile="";
    	try(BufferedReader in = new BufferedReader(
                new InputStreamReader(new URL("https://www.swiggy.com/robots.txt").openStream()))) {
            String line = null;
            
            
            while((line = in.readLine()) != null) {
                System.out.println(line);
                robotfile = robotfile.concat(line); 
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    	
    	//Checking if the robot.txt file with the text allow, disallow etc
    	if(robotfile.isEmpty()){
    		System.out.println("Robot.txt is empty");
    		String failure=targeturl+": "+CLASS_NAME;
    		sr.totalFail(failure);
    	}
    	else{
    		if(robotfile.contains("User-agent:") && robotfile.contains("Allow:") && robotfile.contains("Sitemap:")
    				&& robotfile.contains("Disallow:"))
    			System.out.println("Robot.txt is present");
    		sr.totalPass();
    	}
    }
    }

